import Vuex from 'vuex';

import jsonData from '~/static/data/data.json';

const createStore = () => {
  return new Vuex.Store({
    state: {
      counter: 0,
      lang: 'en',
      header: jsonData.header.links,
      headerEs: jsonData.header_es.links,
      headerPt: jsonData.header_pt.links,
      mobileMenu: false,
      footer: jsonData.footer,
      footerEs: jsonData.footerEs,
      footerPt: jsonData.footerPt
    },
    mutations: {
      increment (state) {
        state.counter++;
      },
      toggle_menu(state) {
        state.mobileMenu = !state.mobileMenu;
      },
      lang_change(state, payload) {
        state.lang = payload.lang;
      },
    },
  })
};

export default createStore;
